#!/usr/bin/env python

import argparse

from core import db
from core.utils import Utils

def parse_args( args=None ):
	parser = argparse.ArgumentParser(description='all bases r belong to u')
	parser.add_argument('-u', '--host', help='host', type=str,
						metavar='www.domain.tld', default=None )
	opts = parser.parse_args( args=args )
	return opts

def stats( args ):
	utils = Utils()

	print '[*] Hosts: {}'.format( utils.count_hosts(host=args.host) )
	print '[*] Requests: {}'.format( utils.count_requests(host=args.host) )
	print '[*] Fuzzed: {}'.format( utils.count_fuzzed(host=args.host) )
	print '[*] Detects: {}'.format( utils.count_detects(host=args.host) )

def main(): stats(parse_args())

if __name__ == "__main__":
	main()
