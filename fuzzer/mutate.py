#!/usr/bin/env python

from protocol import CookieField, HeaderField, GETField, POSTField, PathField

class Mutation( object ): pass
	
class IEUA( Mutation ):

	def __call__( self, request ):
		HeaderField.sub(request,
						key = 'User-Agent',
						value = 'Mozilla/5.0 ' \
				'(compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)' )
		
class Mutate( object ):
	
	def __init__( self, mutations = [ IEUA(), ] ):
		self.mutations = mutations

	def __call__( self, request ):
		[ mutate(request) for mutate in self.mutations ]
