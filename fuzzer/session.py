#!/usr/bin/env python

from collections import deque

from time import time
from requests import Session
from requests.adapters import HTTPAdapter
from requests.exceptions import Timeout

class SessionsExhausted( Exception ): pass

class Sessions( object ):

	def __init__( self, sessions = 1, retries = 3, timeout=60, rtt = 5 ):
		self.authed = deque()
		self.unauthed = deque()

		adapter = HTTPAdapter( max_retries=retries )
		for ii in xrange( sessions ):
			session = Session()
			session.mount( 'http', adapter )
			self.unauthed.append( session )

		self.timeout = timeout
		self.rtt = rtt

	def acquire( self, authed ):
		if authed:
			try:
				now = time()
				while True:
					last,session=self.authed.popleft()
					if(( now + self.rtt ) < ( last + self.timeout )):
						return session
					else:
						self.unauthed.append( session )
			except IndexError:
				raise SessionsExhausted()

		else:
			try:
				return self.unauthed.popleft()
			except IndexError:
				raise SessionsExhausted()

	def release( self, session, authed ):
		if authed:
			self.authed.append((time(), session ))
		else:
			self.unauthed.append( session )
