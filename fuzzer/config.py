#!/usr/bin/env python

config = {

	'sleep_key':'@SLEEP',
	'sleep_val': 15,

	'alt_nullbyte_key': '@ALT_NULLBYTE',
	'alt_nullbyte_val': '/.'*255,

	'sql_eoq_key': '@SQL_EOQ',
	'sql_eoq_val': ';-- ',

	'relocate_key': '@RELOCATE',

	'referer': '127.0.0.1',

}

BAD_CODES = [ 400, 414, 502 ]

SUSPICIOUS_CODES = [ 301, 302, 303, 307, 503 ]

OK_CODES = [ 200, 404, 403 ]

TIMEOUT_CODE = 000

ROUNDTRIP = 5

TIMEOUT = config['sleep_val'] - ROUNDTRIP

REQUEST_TOO_LARGE = 1024*8 + 1
