#!/usr/bin/env python

import re
import difflib
import logging

from itertools import chain
from functools import wraps
from traceback import format_exc

from config import TIMEOUT, OK_CODES, SUSPICIOUS_CODES, BAD_CODES, TIMEOUT_CODE

def safe( func ):

	@wraps( func )
	def decorator( *args, **kwargs ):

		try:
			return func( *args, **kwargs )
		except:
			logging.warn('recovering @{} from {}'.format(func,format_exc()))

	return decorator

def valid( result ):
	return filter( lambda x: True if x is not None else False, [
			result.pages.uri_too_large,
			result.pages.rel_not_found,
			result.pages.rel_index,
			result.pages.not_found,
			result.pages.original,
			result.pages.valid,
			result.pages.index,
			result.pages.null,
	])

def get_fuzzed( result ):
	return result.pages.fuzzed

def get_valid( result ):
	return result.pages.valid or result.pages.original

def get_uri_too_large( result ):
	return result.pages.uri_too_large

def ratio( new, old ):
	return difflib.SequenceMatcher(None,new,old).ratio()

def simular( new, old, simularity ):
	if( ratio( new.response.content, old.response.content ) > simularity ):
		return True
	return False

@safe
def detect_time( result ):

	name = 'time'

	vuln = False

	rtt = TIMEOUT

	try:
		rtt = get_fuzzed( result ).response.rtt
		if rtt > TIMEOUT:
			vuln = True

	except AttributeError:
		vuln = True

	if vuln:
		result.vulnerable( anomoly=name, detect=rtt )

def listdiff( l1, l2, default = [] ):
	sequences = difflib.SequenceMatcher( None, l1, l2 )
	return reduce( lambda p, q: p + q,
		map( lambda t: sequences.a[ t[1]: t[2] ],
			filter( lambda x: x[0] != 'equal',
				sequences.get_opcodes() ) ), default )

class ContextCache( dict ):

	def get_and_set( self, key, f, args = (), kwargs = {} ):

		try:
			result = self[ key ]

		except KeyError:
			result = f( *args, **kwargs )
			self[ key ] = result

		finally:
			return result

@safe
def detect_sequence( result, detects ):

	name = 'sequence'

	fuzzed_content = get_fuzzed( result ).response.content
	valid_content = get_valid( result ).response.content

	context = ContextCache()

	for detect in detects:

		fuzzed_matches = detect.findall( fuzzed_content, context )
		valid_matches = detect.findall( valid_content, context )

		anomolies = listdiff( fuzzed_matches, valid_matches )

		for anomoly in anomolies:
			result.vulnerable( anomoly=name,
								detect='{}'.format( detect.search_string ))

@safe
def detect_diff( result, simularity ):

	name = 'diff'

	fuzzed = get_fuzzed( result )

	matches = [simular(fuzzed,page,simularity) for page in valid(result)]

	if( len( matches ) > 0
			and not any(matches)
			and fuzzed.response.code not in BAD_CODES ):

		result.vulnerable( anomoly=name, detect=simularity )

		#result.vulnerable( anomoly=name,
							#detect="\n"+'-'*80+"\n{} {}\n{}\n{}" \
									#.format(fuzzed.response.code,
											#simularity,
											#get_valid(result) \
													#.response.content,
											#fuzzed.response.content ))

@safe
def detect_code( result ):

	name = 'code'

	code = get_fuzzed( result ).response.code

	get_code = lambda x: x.response.code\
						if x.response is not None\
						else TIMEOUT_CODE

	is_valid_code = lambda x: True if x in\
					chain([get_code(x) for x in valid(result)],[TIMEOUT_CODE,])\
					else False

	if code in SUSPICIOUS_CODES:
		result.suspicious( anomoly=name, detect=code )

	elif( not is_valid_code( code ) and code not in BAD_CODES ):
		result.vulnerable( anomoly=name, detect=code )

@safe
def detect_uri_too_large( result ):

	uri_too_large_content = get_uri_too_large( result ).response.content

	fuzzed = get_fuzzed( result )
	fuzzed_content = fuzzed.response.content

	if uri_too_large_content == fuzzed_content:
		logging.error( '414 Request-URI Too Large @{}' \
						.format(result.to_string()) )

def detect( result, detects, simularity ):
	detect_sequence( result, detects )
	detect_diff( result, simularity )
	detect_time( result )
	detect_code( result )
	detect_uri_too_large( result )
	result.save()
