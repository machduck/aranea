#!/usr/bin/env python

from config import config as dconf

from loader import Loader
from protocol import HTTPProtocol
from utils import Utils
from detect import detect

from collections import namedtuple

Instance = namedtuple( 'Instance', ['page',
									'field',
									'key',
									'value',
									'fuzz' ])

class Fuzzer( object ):

	def __init__( self,
					config=None,
					filters=[],
					types=[ 'get', 'post', 'path', 'cookie', 'header' ],
					fuzz=[ 'sql', 'xss', 'cmd', 'lfi' ],
					simularity=0.95 ):

		self.config = config or dconf
		self.loader = Loader( types=fuzz ).prepare_all( **self.config )
		self.utils = Utils()
		self.protocol = HTTPProtocol( types=types )
		self.filters = filters
		self.simularity = simularity

	def fuzz_host( self, host ):
		for page in self.utils.requests( host=host ):
			for instance in self.fuzz_page( page ):
				yield instance

	def fuzz_page( self, page ):
			for sub,key,value in self.protocol.fields( page.request ):
				for fuzz in self.loader.fuzz():
					fuzz.bind_fuzz( page, **self.config )
					instance = Instance(page,sub,key,value,fuzz)
					if all([ f( instance ) for f in self.filters ]):
						yield( instance )

	def detect( self, result ):
		detect( result, self.loader.detect(), self.simularity )

	def results( self, *args, **kwargs ):
		for result in Utils.fuzzed( *args, **kwargs ):
			yield result
