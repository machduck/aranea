#!/usr/bin/env python

from itertools import chain
from config import config

class HTTPProtocol( object ):

	@classmethod
	def register( cls, typecls ):
		typecls.register( cls.get_types() )

	@classmethod
	def get_types( cls ):
		return cls._all

	_all = {}

	def __init__( self,	types=None ):
		self.types = types or HTTPProtocol.get_types().keys()

	def groups( self, request ):
		for cls in self.types:
			yield( HTTPProtocol.get_types()[cls]().fields(request) )

	def fields( self, request ):
		for group in self.groups( request ):
			for field in group:
				yield field

	@classmethod
	def field( cls, typecls ):
		cls.register( typecls )
		return typecls

class ProtocolField( object ):

	@classmethod
	def register( cls, dct ):
		dct[ cls.get_name() ] = cls

	@classmethod
	def get_name( cls ):
		return cls.name

@HTTPProtocol.field
class GETField( ProtocolField ):

	name = 'get'

	@classmethod
	def fields( cls, request ):
		for key,value in request.params.iteritems():
			yield( (cls,key,value) )

	@staticmethod
	def sub( request, key, value ):
		request.params[ key ] = value

@HTTPProtocol.field
class GETKeyField( GETField ):

	name = 'get_key'

	@staticmethod
	def sub( request, key, value ):
		old_value = request.params.pop( key )
		request.params[ value ] = old_value

@HTTPProtocol.field
class POSTField( ProtocolField ):

	name = 'post'

	@staticmethod
	def sub( request, key, value ):
		request.data[ key ] = value

	@classmethod
	def fields( cls, request ):
		for key,value in request.data.iteritems():
			yield( (cls,key,value) )

@HTTPProtocol.field
class POSTKeyField( POSTField ):

	name = 'post_key'

	@staticmethod
	def sub( request, key, value ):
		old_value = request.data.pop( key )
		request.data[ value ] = old_value

@HTTPProtocol.field
class HeaderField( ProtocolField ):

	name = 'header'

	extra = {
		'X-Real-IP': '127.0.0.1',
		'X-Forwarded-For': '127.0.0.1',
		'X-Requested-With': 'XMLHttpRequest',
		'Referer': 'https://{}/'.format( config['referer'] ),
		'User-Agent': 'Mozilla/5.0',
	}

	whitelist = [
		'Referer',
		'User-Agent',
		'Host',
	]

	@classmethod
	def fields( cls, request ):

		def whitefilter( kvpair ):
			key,value = kvpair
			if any([ key.lower()==header.lower() for header in cls.whitelist ]):
				return True

		for key,value in chain( filter(whitefilter,request.headers.iteritems()),
								[(k,v) for (k,v) in cls.extra.iteritems()
									if k not in request.headers.iterkeys() ]):
			yield( (cls,key,value) )

	@staticmethod
	def sub( request, key, value ):
		request.headers[ key ] = value

@HTTPProtocol.field
class PathField( ProtocolField ):

	name = 'path'

	@staticmethod
	def sub( request, key, value ):
		request.path[ key ] = value

	@classmethod
	def fields( cls, request ):
		for key,value in enumerate(request.path):
			yield( (cls,key,value) )

@HTTPProtocol.field
class CookieField( ProtocolField ):

	name = 'cookie'

	@staticmethod
	def sub( request, key, value ):
		request.cookies[ key ] = value

	@classmethod
	def fields( cls, request ):
		for key,value in request.cookies.iteritems():
			yield( (cls,key,value) )
