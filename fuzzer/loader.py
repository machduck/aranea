#!/usr/bin/env python

import yaml
import os
import re

from lxml import html
from os.path import splitext

from config import config

import logging

class Entity( yaml.YAMLObject ):

	def get_name( self ):
		return self.name

class FuzzEntity( Entity ):

	def prepare_fuzz( self, *args, **kwargs ):
		pass

	def bind_fuzz( self, *args, **kwargs ):
		pass

class DetectEntity( Entity ):

	@property
	def search_regex( self ):
		return self.regex

	def prepare_detect( self, **kwargs ):
		self.regex = re.compile(re.escape( self.search_string ))

	def findall( self, content, context = {} ):
		return self.search_regex.findall( content )

class MutateMixin( object ):

	def mutate( self, value ):

		if self.action == 'append':
			return value + self.payload

		elif self.action == 'prepend':
			return self.payload + value

		elif self.action == 'overwrite':
			return self.payload

		elif self.action == 'relocate':
			return self.payload.replace( config['relocate_key'], value )

class Loader( object ):

	@classmethod
	def register( cls, typecls ):
		typecls.register( cls.get_types() )

	@classmethod
	def get_types( cls ):
		return cls._all

	_all = {}

	@classmethod
	def type( cls, typecls ):
		cls.register( typecls )
		return typecls

	def __init__( self, path = 'data', types = None ):
		self._fuzz = []
		self._detect = []
		for type in types or Loader.get_types():
			with open(os.path.join(path,'{}.yaml'.format(type)),'rb') as fh:
				loader = Loader.get_types()[type]( fh )
				self._fuzz += loader.fuzz()
				self._detect += loader.detect()

	def fuzz( self ):
		for fuzz in self._fuzz:
			yield fuzz

	def detect( self ):
		for detect in self._detect:
			yield detect

	def prepare_all( self, *args, **kwargs ):
		for fuzz in self.fuzz():
			fuzz.prepare_fuzz( *args, **kwargs )

		for detect in self.detect():
			detect.prepare_detect( *args, **kwargs )

		return self

class XSS( FuzzEntity, DetectEntity, MutateMixin ):
	yaml_tag = '!XSS'

	name = 'xss'

	@property
	def search_string( self ):
		return self.search

	def findall( self, content, context ):

		type = self.detect

		if type == 'tag':
			return super( XSS, self ).findall( content )

		elif type == 'attribute':
			root = context.get_and_set( self.name,
										html.fromstring,
										args=(content,) )
			search_key, search_val = self.search_string.split( '=', 1 )
			#xpath = './/*[@{}="$val"]'.format( search_key )
			xpath = './/*[@*[name()="$key"] = "$val"]'
			return root.xpath( xpath, key=search_key, val=search_val )

	def __repr__( self ):
		return '<XSS {}>'.format( self.payload )

class SQL( object ):

	name = 'sqli'

class SQLi( FuzzEntity, MutateMixin, SQL ):
	yaml_tag = '!SQLi'

	def prepare_fuzz(self,sleep_val,sleep_key,sql_eoq_key,sql_eoq_val,**kwargs):
		self.payload = self.payload.replace( str(sleep_key), str(sleep_val) )
		self.payload = self.payload.replace(str(sql_eoq_key), str(sql_eoq_val))

	def __repr__( self ):
		return '<SQLi {}>'.format( self.payload )

class SQLError( DetectEntity, SQL ):
	yaml_tag = '!SQLError'

	@property
	def search_string( self ):
		return self.data

class Loadable( object ):
	
	@classmethod
	def register( cls, dct ):
		dct[ cls.get_filename() ] = cls

	@classmethod
	def get_filename( cls ):
		return cls.file

@Loader.type
class SQLiLoader( Loadable ):

	file = 'sql'

	def __init__( self, fh ):
		self.data = yaml.load( fh )

	def fuzz( self ):
		return self.data['fuzz']

	def detect( self ):
		return self.data['detect']

@Loader.type
class XSSLoader( Loadable ):

	file = 'xss'

	def __init__( self, fh ):
		self.data = yaml.load( fh )

	def fuzz( self ):
		return self.data['xss']

	def detect( self ):
		return self.data['xss']

@Loader.type
class LFILoader( Loadable ):

	file = 'lfi'

	def __init__( self, fh ):
		self.data = yaml.load( fh )

	def fuzz( self ):
		return self.data['fuzz']

	def detect( self ):
		return self.data['detect']

class LFI( object ):

	name = 'lfi'

class LFII( FuzzEntity, MutateMixin, LFI ):
	yaml_tag = '!LFI'

	def prepare_fuzz( self, alt_nullbyte_key, alt_nullbyte_val, **kwargs ):
		self.payload = self.payload.replace(str(alt_nullbyte_key),
											str(alt_nullbyte_val))

	def __repr__( self ):
		return '<LFI {}>'.format( self.payload )

class LFIError( DetectEntity, LFI ):
	yaml_tag = '!LFIError'

	@property
	def search_string( self ):
		return self.data

@Loader.type
class CMDLoader( Loadable ):

	file = 'cmd'

	def __init__( self, fh ):
		self.data = yaml.load( fh )

	def fuzz( self ):
		return self.data['cmd']

	def detect( self ):
		return []

class CMD( FuzzEntity, DetectEntity, MutateMixin ):
	yaml_tag = '!CMD'

	name = 'cmd'

	def prepare_fuzz( self, sleep_val, sleep_key, **kwargs ):
		self.payload = self.payload.replace( str(sleep_key), str(sleep_val) )

	def __repr__( self ):
		return '<CMD {}>'.format( self.payload )
