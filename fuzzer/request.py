#!/usr/bin/env python

import logging
import traceback
import random
import string

from session import SessionsExhausted
from session import Timeout

from time import time
from copy import deepcopy
from os.path import splitext
from functools import wraps

from core.models import Fuzzable
from core.models import Response

from mutate import Mutate, IEUA

from config import REQUEST_TOO_LARGE

def nonexisting():
	return ''.join([random.choice( string.letters + string.digits )
					for x in xrange(random.randrange(0x10,0x20))])

def prepare( func ):
	@wraps( func )
	def decorator( self, *args, **kwargs ):
		return func(self,*args,page=deepcopy(self.instance.page),**kwargs)
	return decorator

def authenticate( func ):
	@wraps( func )
	def decorator( self, page, *args, **kwargs ):

		try:
			session = self.store.acquire(authed=page.doauth)
			mod_auth = False
		except SessionsExhausted:
			session = self.store.acquire(authed=not page.doauth)
			mod_auth = True

		if mod_auth:

			if page.doauth:
				pass # XXX authenticate & csrfify auth

			else:
				pass # XXX deauethenticate & csrfify deauth

		result = func( self,
						*args,
						page = page,
						session = session,
						**kwargs )

		self.store.release( session, page.doauth )

		return result
	return decorator

def csrf_token( func ):
	@wraps( func )
	def decorator( self, page, session, *args, **kwargs ):

		if page.docsrf:
			pass # XXX csrfify

		return func(self,
					*args,
					page = page,
					session = session,
					**kwargs)

	return decorator

class Request( object ):

	def __init__( self, instance, store, mutations=[ IEUA(), ] ):
		self.instance = instance
		self.store = store
		self.result = Fuzzable( host=self.instance.page.host,
								field=self.instance.field.get_name(),
								param=self.instance.key,
								type=self.instance.fuzz.get_name(),
								payload=self.instance \
											.fuzz \
											.mutate(self.instance.value))
		self.mutate = Mutate( mutations=mutations )

	@prepare
	@authenticate
	@csrf_token
	def null( self, page, session ):

		self.instance.field.sub(page.request,
								self.instance.key,
								'')

		try:
			self.result.pages.null = self.send_facade( page, session )
		except Timeout:
			logging.warning( '{}'.format( traceback.format_exc() ))
		except:
			logging.error( '{}'.format( traceback.format_exc() ))

	@prepare
	@authenticate
	@csrf_token
	def valid( self, page, session ):

		try:
			self.result.pages.valid = self.send_facade( page, session )
		except Timeout:
			logging.warning( '{}'.format( traceback.format_exc() ))
		except:
			logging.error( '{}'.format( traceback.format_exc() ))

	@prepare
	@authenticate
	@csrf_token
	def not_found( self, page, session ):
		
		page.request.path = [ nonexisting(), ]

		try:
			self.result.pages.not_found = self.send_facade( page, session )
		except Timeout:
			logging.warning( '{}'.format( traceback.format_exc() ))
		except:
			logging.error( '{}'.format( traceback.format_exc() ))

	@prepare
	@authenticate
	@csrf_token
	def index( self, page, session ):

		page.request.path = []

		try:
			self.result.pages.index = self.send_facade( page, session )
		except Timeout:
			logging.warning( '{}'.format( traceback.format_exc() ))
		except:
			logging.error( '{}'.format( traceback.format_exc() ))

	@prepare
	@authenticate
	@csrf_token
	def uri_too_large( self, page, session ):

		page.request.path = [ '?{}'.format('/'*REQUEST_TOO_LARGE ), ]

		try:
			self.result.pages.uri_too_large = self.send_facade( page, session )
		except Timeout:
			logging.warning( '{}'.format( traceback.format_exc() ))
		except:
			logging.error( '{}'.format( traceback.format_exc() ))

	@prepare
	@authenticate
	@csrf_token
	def rel_not_found( self, page, session ):

		try:
			file,ext = splitext( page.request.path[-1] )
		except IndexError:
			ext = ''
		page.request.path[-1] = nonexisting() + ext

		try:
			self.result.pages.rel_not_found = self.send_facade( page, session )
		except Timeout:
			logging.warning( '{}'.format( traceback.format_exc() ))
		except:
			logging.error( '{}'.format( traceback.format_exc() ))

	@prepare
	@authenticate
	@csrf_token
	def rel_index( self, page, session ):

		page.request.path.pop()

		try:
			self.result.pages.rel_index = self.send_facade( page, session )
		except Timeout:
			logging.warning( '{}'.format( traceback.format_exc() ))
		except:
			logging.error( '{}'.format( traceback.format_exc() ))

	def csrf( self ): pass

	def auth( self ): pass

	def deauth( self ): pass

	def reflection( self, reflection ): pass

	def reflections( self ): yield StopIteration()

	def send_facade( self, page, session ):
		self.mutate( page.request )
		start = time()
		result = self.send( page, session )
		end = time()
		rtt = end-start
		logging.debug( '{}\n{}\n{}'.format( result.status_code,
											result.content,
											'-'*80 ))
		page.response=Response( content = result.content,
								headers = result.headers,
								code = result.status_code,
								cookies = result.cookies,
								rtt = rtt )
		return page

	def send( self, page, session ):
		dport = lambda port: ':'+str(port) if port != 80 else ''
		result = session.request(
				page.method,
				page.scheme + '://' + page.host + dport( page.port ) + '/' \
						+ '/'.join( page.request.path ),
				params = page.request.params,
				data = page.request.data,
				headers = page.request.headers,
				cookies = page.request.cookies,
				timeout = 30,
				auth = (),
				allow_redirects = True,
				verify = False,
				proxies = None,
				files = None
		)
		return result

	@prepare
	@authenticate
	@csrf_token
	def fuzz( self, page, session ):

		self.instance.field.sub(page.request,
								self.instance.key,
								self.instance.fuzz.mutate(self.instance.value))

		try:
			self.result.pages.fuzzed = self.send_facade( page, session )
		except Timeout:
			logging.warning( '{}'.format( traceback.format_exc() ))
		except:
			logging.error( '{}'.format( traceback.format_exc() ))

	def __call__( self ):
		return self.result
