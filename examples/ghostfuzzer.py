#!/usr/bin/env python

from gevent.monkey import patch_all; patch_all()

from core import db
from core.models import WebPage, Fuzzable

Fuzzable.drop_collection()
WebPage.drop_collection()

WebPage.from_url( 'php5fpm.nginx/test.php?q=1&param=two' ).save()

from fuzz import GHostFuzzer

fuzz = GHostFuzzer( workers=10,
					fuzz=['sql','xss'],
					types=['get','post','path','cookie','header'],
					simularity=0.9 )
fuzz.fuzz('php5fpm.nginx')
fuzz.join()

for result in fuzz.results('php5fpm.nginx'):
	print '[*] {}'.format( result.to_string() )
	for detect in result.data:
		print '[!] {}'.format( detect.to_string() )
