#!/usr/bin/env python

from nose.tools import assert_equals

from fuzzer.config import config
from fuzzer.loader import Loader, Entity

import types

from fuzzer.protocol import GETField
from core.models import WebPage, Fuzzable
from tests.test_models import Data

from itertools import ifilter

from fuzzer.protocol import HTTPProtocol
from fuzzer.utils import Utils
from fuzzer.mutate import Mutate, IEUA
from fuzzer.fuzzer import Fuzzer
from fuzzer.session import Sessions, SessionsExhausted

from fuzz import GHostFuzzer, parse_args

class TestLoader( object ):

	def setup( self ):
		self.loader = Loader()
		self.loader.prepare_all(**config)

	def teardown( self ):
		del self.loader

	def test_loader( self ):
		assert_equals( isinstance( 	self.loader.detect(), types.GeneratorType ),
									True )
		assert_equals( isinstance( 	self.loader.fuzz(), types.GeneratorType ),
									True )

		for fuzz in self.loader.fuzz():
			assert_equals(isinstance(fuzz,Entity),True)

		for detect in self.loader.detect():
			assert_equals(isinstance(detect,Entity),True)

class TestGETField( object ):
	SUB = 'SUB'

	def setup( self ):
		self.page = WebPage.from_url( url=Data.URL, post=Data.POST )
		self.get = GETField()

	def teardown( self ):
		del self.page
		del self.get

	def test_generator( self ):
		assert_equals( isinstance( self.get.fields( self.page.request ), 
												types.GeneratorType ), 
									True )
		fields = set([ (key,value)
					for sub,key,value in self.get.fields( self.page.request ) ])
		original = set([ (k,v) for k,v in Data.QUERY_DATA.iteritems() ])
		assert_equals( fields, original )

		subs = [ sub for sub,key,value in self.get.fields( self.page.request ) ]
		for sub in subs:
			assert_equals( issubclass( sub, GETField ), True )

	def test_sub( self ):
		for sub,key,value in self.get.fields( self.page.request ):
			sub.sub( self.page.request, key, TestGETField.SUB )

		fields = set([ (key,value)
						for sub,key,value 
						in self.get.fields( self.page.request ) ])
		original = set([ (key,TestGETField.SUB)
						for key,value in Data.QUERY_DATA.iteritems() ])

		assert_equals( fields, original )

class TestHTTPProtocol( object ):

	def setup( self ):
		self.protocol = HTTPProtocol()
		self.page = WebPage( url=Data.URL, post=Data.POST )

	def teardown( self ):
		del self.protocol
		del self.page

	def test_protocol( self ):
		for group in self.protocol.groups( self.page.request ):
			assert_equals( isinstance( group, types.GeneratorType ), True )

class TestUtils( object ):

	def setup( self ):
		self.utils = Utils()

	def teardown( self ):
		del self.utils
		WebPage.drop_collection()

	def test_utils( self ):
		WebPage.from_url( url=Data.URL, post=Data.POST ).save()
		WebPage.from_url( url=Data.LOCAL_URL ).save()
		assert_equals(len([x for x in self.utils.requests(host=Data.DOMAIN)]),1)

	def test_filter( self ):
		def generator():
			for x in xrange(5):
				yield x

		odd = lambda x: True if x%2==1 else False

		assert_equals(len(filter( odd, generator())),2)

		filters = [ odd, lambda x: True if x%3==0 else False ]
		g = generator()
		for f in filters:
			g = ifilter( f, g )
		results = [ x for x in g ]

		assert_equals( len(results), 1 )
		assert_equals( results[0], 3 )

class TestMutate( object ):

	def setup( self ):
		self.mutate = Mutate( mutations=[ IEUA(), ] )

	def teardown( self ):
		del self.mutate

	def test_mutate( self ):

		class SQLiMock( object ):
			def __init__( self, payload ):
				self.payload = payload

		page = WebPage.from_url( url=Data.URL, post=Data.POST )
		header = 'User-Agent'
		value = 'Mozilla/5.0 ' \
				+ '(compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)'
		self.mutate( page.request )
		assert_equals( page.request.headers[ header ], value )

class TestFuzzer( object ):

	def setup( self ):
		self.fuzzer = Fuzzer()
		WebPage.from_url( url=Data.URL, post=Data.POST ).save()
		WebPage.from_url( url=Data.LOCAL_URL ).save()

	def teardown( self ):
		del self.fuzzer
		WebPage.drop_collection()

	def test_fuzzer( self ):
		assert_equals( isinstance( self.fuzzer.fuzz_host( Data.DOMAIN ),
									types.GeneratorType),
						True)

class TestSession( object ):

	def setup( self ):
		self.sessions = Sessions(0)

	def teardown( self ):
		del self.sessions

	def test_session( self ):
		self.sessions.release( 1, True )
		self.sessions.release( 2, True )
		self.sessions.release( 3, False )

		assert_equals( self.sessions.acquire( True ), 1 )
		assert_equals( self.sessions.acquire( False ), 3 )
		assert_equals( self.sessions.acquire( True ), 2 )

class FuzzMock( object ):
	DOMAIN = 'php5fpm.nginx'
	BASE_URL = 'http://{}/'.format( DOMAIN )

class TestFuzz( object ):

	def setup( self ):
		WebPage.from_url(
				url='{}test.php?one=1&two=2'.format(FuzzMock.BASE_URL)
		).save()

	def teardown( self ):
		WebPage.drop_collection()
		Fuzzable.drop_collection()

	def test_fuzz( self ):
		args = parse_args( args=['--host', FuzzMock.DOMAIN ])
		fuzz = GHostFuzzer( args.threads,
							fuzz=args.fuzz,
							types=args.types,
							simularity=args.simularity )
		fuzz.fuzz( args.host )
		fuzz.join()
