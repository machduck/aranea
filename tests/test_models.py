#!/usr/bin/env python

from nose.tools import assert_equals

import core.db
from core.models import WebPage

class Data( object ):
	SCHEME = 'http'
	PATH_ARRAY = [ 'path', 'file' ]
	PATH = '/' + '/'.join( PATH_ARRAY )
	QUERY_DATA = {'q': '1', 'a': '2', 'b': '3'}
	QUERY = '&'.join([ '='.join(x) for x in QUERY_DATA.iteritems() ])
	POST_DATA = {'p': '1', 'a': '2', 'c': '4'}
	POST = '&'.join([ '='.join(x) for x in POST_DATA.iteritems() ])
	DOMAIN = 'www.site.tld'
	URL = '{}://{}{}?{}'.format( SCHEME, DOMAIN, PATH, QUERY )
	HEADERS = { 'X-Test': 'True' }
	METHOD = 'post'

	LOCAL_URL = 'http://127.0.0.1/'

class TestWebPage( object ):

	CLEAR = True

	def setup( self ):
		if TestWebPage.CLEAR:
			WebPage.drop_collection()

	def teardown( self ):
		if TestWebPage.CLEAR:
			WebPage.drop_collection()

	def test_page( self ):
		WebPage.from_url(url=Data.URL,post=Data.POST,method=Data.METHOD).save()
		for page in WebPage.objects.no_cache().no_dereference():
			assert_equals( page.url, Data.URL )
			assert_equals( page.scheme, Data.SCHEME )
			assert_equals( page.path, Data.PATH )
			assert_equals( page.query, Data.QUERY )
			assert_equals( page.post, Data.POST )
			assert_equals( page.host, Data.DOMAIN )
			assert_equals( page.method, Data.METHOD )

	def test_request( self ):
		webpage = WebPage.from_url( url=Data.URL, post=Data.POST )
		webpage.request.headers = Data.HEADERS
		webpage.save()
		for page in WebPage.objects.no_cache().no_dereference():
			assert_equals( page.request.path, Data.PATH_ARRAY )
			assert_equals( page.request.params, Data.QUERY_DATA )
			assert_equals( page.request.data, Data.POST_DATA )
			assert_equals( page.request.headers, Data.HEADERS )
