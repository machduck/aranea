#!/usr/bin/env python

import argparse

from core import db
from core.models import WebPage

def parse_args( args=None ):
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('-u', '--host', help='host', type=str,
						metavar='www.domain.tld', default=None )
	parser.add_argument('-m','--method', help='method', type=str,
						metavar='get', default=None )
	parser.add_argument('-p','--post', help='post', type=str,
						metavar='get', default=None )
	opts = parser.parse_args( args=args )
	return opts

def request( args ):
	WebPage.from_url( args.host, method=args.method, post=args.post ).save()

def main(): request(parse_args())

if __name__ == "__main__":
	main()
