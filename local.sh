#!/bin/bash

ROOT_PATH='./'

${ROOT_PATH}drop.py
${ROOT_PATH}request.py -u 'php5fpm.nginx/test.php?q=1&b=test'
${ROOT_PATH}fuzz.py -u 'php5fpm.nginx' -v $@
${ROOT_PATH}dump.py -d hosts requests detects
${ROOT_PATH}stats.py
