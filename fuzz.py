#!/usr/bin/env python

import gevent.monkey
gevent.monkey.patch_all()

from gevent.pool import Pool
from gevent.lock import BoundedSemaphore
from gevent.queue import JoinableQueue as Queue

from fuzzer.fuzzer import Fuzzer
from fuzzer.request import Request
from fuzzer.session import Sessions

from core import db, log

import argparse
import logging

from random import choice

from functools import wraps

def sync( func ):
	@wraps( func )
	def decorator( self, *args, **kwargs ):
		with self.lock:
			return func( self, *args, **kwargs )
	return decorator

class GSessions( Sessions ):

	def __init__( self, *args, **kwargs ):
		self.lock = BoundedSemaphore()
		super( GSessions, self ).__init__( *args, **kwargs )

	@sync
	def acquire( self, *args, **kwargs ):
		return super( GSessions, self ).acquire( *args, **kwargs )

	@sync
	def release( self, *args, **kwargs ):
		return super( GSessions, self ).release( *args, **kwargs )

def concurrent( func ):
	@wraps( func )
	def decorator( self, *args, **kwargs ):
		with self.concurrency:
			return func( self, *args, **kwargs )
	return decorator

class GRequest( Request ):

	def __init__( self, concurrency, *args, **kwargs ):
		super( GRequest, self ).__init__( *args, **kwargs )
		self.pool = Pool()
		self.concurrency = concurrency

	def __call__( self ):
		[ self.pool.spawn(job) for job in [
			self.valid,
			self.null,
			self.index,
			self.not_found,
			self.rel_index,
			self.rel_not_found,
			self.uri_too_large,
		]]
		self.pool.join()
		self.fuzz()
		[ self.pool.spawn(self.reflection, reflection)
				for reflection in self.reflections() ]
		self.pool.join()
		return super( GRequest, self ).__call__()
	
	@concurrent
	def rel_not_found( self, *args, **kwargs ):
		return super( GRequest, self ).rel_not_found( *args, **kwargs )

	@concurrent
	def rel_index( self, *args, **kwargs ):
		return super( GRequest, self ).rel_index( *args, **kwargs )

	@concurrent
	def not_found( self, *args, **kwargs ):
		return super( GRequest, self ).not_found( *args, **kwargs )

	@concurrent
	def index( self, *args, **kwargs ):
		return super( GRequest, self ).index( *args, **kwargs )

	@concurrent
	def valid( self, *args, **kwargs ):
		return super( GRequest, self ).valid( *args, **kwargs )

	@concurrent
	def null( self, *args, **kwargs ):
		return super( GRequest, self ).null( *args, **kwargs )
	
	@concurrent
	def fuzz( self, *args, **kwargs ):
		return super( GRequest, self ).fuzz( *args, **kwargs )

	@concurrent
	def uri_too_large( self, *args, **kwargs ):
		return super( GRequest, self ).uri_too_large( *args, **kwargs )

def parse_args( args=None ):
	parser = argparse.ArgumentParser(
			description=choice(('u fuzz br0?', 'u want sum fuz?'))
	)
	parser.add_argument('-v', '--verbosity', action='store_true',
						dest='verbosity', help='verbosity')
	parser.add_argument('-u', '--host', help='host', type=str,
						metavar='www.domain.tld', default=None )
	parser.add_argument('-w', '--workers', help='workers', type=int, default=3,
						metavar=3)
	parser.add_argument('-s', '--simularity', help='simularity metric',
						type=float,	default=0.95, metavar=0.95)
	parser.add_argument('-f', '--fuzz', help='fuzz', type=str, nargs='+',
						default=[ 'sql', 'xss', 'lfi', 'cmd' ], 
						metavar=(chr(0x8),'sql, xss, lfi, cmd'))
	parser.add_argument('-t', '--types', help='types', type=str, nargs='+',
						default=[ 'get', 'post', 'path', 'cookie', 'header' ],
						metavar=(chr(0x8),'get, post, path, cookie, header'))
	opts = parser.parse_args( args=args )
	return opts

class GFuzzer( Fuzzer ):

	def __init__( self, workers, *args, **kwargs ):
		super( GFuzzer, self ).__init__( *args, **kwargs )
		self.pool = Pool( workers )
		self.store = GSessions( workers )
		self.concurrency = BoundedSemaphore( workers )
		self.queue = Queue( workers )
		[self.pool.spawn( GFuzzer.worker, self ) for x in xrange( workers )]

	def join( self ):
		self.queue.join()

	@staticmethod
	def worker( fuzzer ):
		for instance in fuzzer.queue:
			logging.info( '{} {} {}={} {}'.format( instance.page.to_string(),
													instance.field.get_name(),
													instance.key,
													instance.value,
													instance.fuzz ))
			request = GRequest( fuzzer.concurrency,
								instance,
								store=fuzzer.store )
			fuzzer.detect( request() )
			fuzzer.queue.task_done()

class GPageFuzzer( GFuzzer ):

	def fuzz( self, *args, **kwargs ):
		for instance in super( GPageFuzzer, self ).fuzz_page( *args, **kwargs ):
			self.queue.put( instance )

class GHostFuzzer( GFuzzer ):

	def fuzz( self, *args, **kwargs ):
		for instance in super( GHostFuzzer, self ).fuzz_host( *args, **kwargs ):
			self.queue.put( instance )

if __name__ == "__main__":

	args = parse_args()

	if args.verbosity:
		level = logging.INFO
	else:
		level = logging.WARN

	log.log( level )

	fuzz = GHostFuzzer( args.workers,
						fuzz=args.fuzz,
						types=args.types,
						simularity=args.simularity )
	fuzz.fuzz( args.host )
	fuzz.join()
