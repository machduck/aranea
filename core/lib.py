#!/usr/bin/env python

def split_min( string, pattern, max=0, min=0, default=None, list=None ):
	result = string.split( pattern, max )
	diff = min - len(result)
	if diff > 0:
		if list is not None:
			result = result + list[diff:]
		elif default is not None:
			result = result + [default ]*diff
	return result
