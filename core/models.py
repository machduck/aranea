#!/usr/bin/env python

from mongoengine import Document, EmbeddedDocument
from mongoengine import IntField, DateTimeField, BooleanField
from mongoengine import StringField, URLField
from mongoengine import DictField, MapField, ListField
from mongoengine import BinaryField
from mongoengine import EmbeddedDocumentField, ReferenceField, DynamicField
from datetime import datetime

from urlparse import urlparse
from lib import split_min

import logging
import mmh3

from itertools import chain

class ToString( object ):

	def to_string( self ):
		return self.__repr__()

class Request( EmbeddedDocument ):
	headers = MapField( field=StringField(), default={} )
	path = ListField( field=StringField(), default=[] )
	params = MapField( field=StringField(), default={} )
	data = MapField( field=StringField(), default={} )
	cookies = MapField( field=StringField(), default={} )

class Response( EmbeddedDocument ):
	content = StringField( default = '' ) # XXX BinaryField
	headers = MapField( field=StringField(), default={} )
	code = IntField( default = 200 )
	rtt = IntField()
	cookies = MapField( field=StringField(), default={} )

class Page( Document ):

	meta = {
		'abstract': True,
		'indexes': [ 'host', 'hash' ],
	}

	url = URLField()
	post = StringField( default='' ) # XXX BinaryField
	path = StringField( default='/' )
	query = StringField( default='' )
	scheme = StringField( choices=( 'http', 'https' ), default='http' )
	date = DateTimeField( default = datetime.utcnow() )
	host = StringField()
	request = EmbeddedDocumentField( Request, default=Request )
	response = EmbeddedDocumentField( Response )
	method = StringField( default='get' )
	doauth = BooleanField( default=False )
	docsrf = BooleanField( default=False )
	port = IntField( default=80 )
	hash = IntField( required=True, unique=True )

	@staticmethod
	def extract_kv( query ):
		dct = {}
		if query is not None:
			kvpairs = query.split('&')
			for kvpair in kvpairs:
				key, value = split_min( kvpair, '=', 1, 2, '' )
				if key != '':
					dct[key]=value
		return dct

	@staticmethod
	def extract_path( path ):
		 return filter(	lambda x: True if len(x)>0 else False,
						path.split('/')) or ['',]

	@staticmethod
	def uhash( request ):
		hash_str=''.join(chain( request.params.keys(),
								request.data.keys(),
								request.path,
								request.cookies.keys(),
								request.headers.keys() ))
		return mmh3.hash( hash_str )

	@classmethod
	def from_url( cls, url, post=None, method=None ):
		if not url.startswith( 'http' ):
			url = 'http://' + url

		url_data = urlparse( url )
		scheme = url_data.scheme
		path = url_data.path
		query = url_data.query
		host = url_data.netloc

		request = Request()

		if path is not None:
			request.path = cls.extract_path( path )

		if query is not None:
			request.params = cls.extract_kv( query )

		if post is not None:
			request.data = cls.extract_kv( post )

		if method is None:
			if post is None:
				method = 'get'
			else:
				method = 'post'

		hash = Page.uhash( request )

		return cls( url=url, post=post, method=method,
					scheme=scheme,host=host, path=path, query=query,
					request=request, hash=hash )

	@staticmethod
	def extract_req_cookies( msg ):
		return {} # XXX

	@staticmethod
	def extract_res_cookies( msg ):
		return {} # XXX

	@staticmethod
	def url_from_proxy( msg ):
		normalize_path = lambda x: x if x != '' else '/'
		return msg.request.scheme + '://' + msg.request.host \
				+ normalize_path( msg.request.path )

	@staticmethod
	def extract_rtt( msg ):
		return 1 # XXX

	@staticmethod
	def extract_ct( obj ):
		return obj.headers.get('Content-Type', ['identity',])[0].split(';')[0]

	@staticmethod
	def extract_ce( object ):
		encoding=object.headers.get('Content-Encoding',['identity',])[0]
		type = encoding.split(';')[0]
		return type

	@classmethod
	def from_proxy( cls, msg, decoder = None ):

		url = cls.url_from_proxy( msg )
		path, query = split_min( msg.request.path, '?', 1, 2, list=['/',''] )
		req_content = msg.request.content
		res_content  = msg.content
		if decoder is not None:
			req_encoding = cls.extract_ce( msg.request )
			res_encoding = cls.extract_ce( msg )
			req_content = decoder.decode( req_encoding, req_content )
			res_content = decoder.decode( res_encoding, res_content )
		dct_post = cls.extract_kv( req_content )
		dct_path = cls.extract_path( path )
		dct_query = cls.extract_kv( query )
		req_dct_cookies = cls.extract_req_cookies( msg )
		res_dct_cookies = cls.extract_req_cookies( msg.request )
		rtt = cls.extract_rtt( msg )

		request=Request(headers=msg.request.headers,
						path=dct_path,
						params=dct_query,
						cookies=req_dct_cookies,
						data=dct_post)

		response = Response(content=res_content,
							code=msg.code,
							headers=msg.headers,
							rtt=rtt,
							cookies=res_dct_cookies)

		hash = Page.uhash( request )

		return cls( method = msg.request.method.lower(),
					port = msg.request.port,
					host = msg.request.host,
					scheme = msg.request.scheme,
					query = query,
					path = path,
					post= req_content,
					url = url,
					request=request,
					response=response,
					hash=hash)

class Token( EmbeddedDocument ):
	token = StringField()
	type = StringField( choices=( 'get', 'post', 'path', 'cookie' ))

class CSRF( EmbeddedDocument ):
	token = StringField()
	type = StringField( choices=( 'get', 'post', 'path', 'header', 'cookie' ))

class Auth( EmbeddedDocument ):
	password = StringField()
	username = StringField()
	type = StringField( choices=( 'get', 'post', 'path', 'cookie' ))

class CSRFPage( Page ):
	csrf = EmbeddedDocumentField( CSRF )

class AuthPage( Page ):
	auth = EmbeddedDocumentField( Auth )
	csrf = ReferenceField( CSRFPage )
	token = EmbeddedDocumentField( Token )

class DeauthPage( Page ):
	csrf = ReferenceField( CSRFPage )
	token = EmbeddedDocumentField( Token )

class WebPage( Page, ToString ):
	csrf = ReferenceField( CSRFPage )
	token = EmbeddedDocumentField( Token )
	auth = ReferenceField( AuthPage )
	deauth = ReferenceField( DeauthPage )

	def __repr__( self ):
		return '<WebPage {} {}>'.format( self.method.upper(), self.url )

class Pages( EmbeddedDocument ):
	original = ReferenceField( WebPage )
	valid = ReferenceField( WebPage )
	not_found = ReferenceField( WebPage )
	index = ReferenceField( WebPage )
	null = ReferenceField( WebPage )
	rel_index = ReferenceField( WebPage )
	rel_not_found = ReferenceField( WebPage )
	fuzzed = ReferenceField( WebPage )
	uri_too_large = ReferenceField( WebPage )

class FuzzData( EmbeddedDocument, ToString ):

	meta = {
		'indexes': [ 'detect', 'anomoly', 'type' ]
	}

	detect = DynamicField( required=True ) # str or int
	anomoly = StringField( choices=( 'sequence', 'diff', 'time', 'code' ),
							required=True )
	type = StringField( choices=( 'vulnerable','suspicious','notice' ),
						required=True )

	def __repr__( self ):
		return '<FuzzData type={}, anomoly={}, detect={}>' \
				.format( self.type, self.anomoly, self.detect )

class Fuzzable( Document, ToString ):

	meta = {
		'indexes': [ 'data', 'host', 'field', 'type', 'param' ]
	}

	host = StringField( required=True )
	field = StringField(required=True,
						choices = ( 'get', 'post', 'header', 'path', 'cookie',
									'get_key', 'post_key' ))
	param = DynamicField( required=True ) # str or int or float
	type = StringField( required=True, choices=( 'sqli', 'xss', 'lfi', 'cmd' ))
	payload = StringField( required=True )

	pages = EmbeddedDocumentField(Pages,default=Pages())
	data = ListField(EmbeddedDocumentField(FuzzData))

	review = BooleanField(default=False)

	def __repr__( self ):
		return '<Fuzzable host={}, field={}, param={}, type={}, payload={}>' \
				.format(self.host,
						self.field,
						self.param,
						self.type,
						self.payload )

	def alert( self, type, anomoly, detect=None ):
		self.data.append( FuzzData( anomoly=anomoly, detect=detect, type=type ))
		self.review = True
		logging.info( '<Alert {} {} {}>'.format( anomoly, type, detect ))

	def vulnerable( self, *args, **kwargs ):
		self.alert( *args, type='vulnerable', **kwargs )

	def suspicious( self, *args, **kwargs ):
		self.alert( *args, type='suspicious', **kwargs )

	def notice( self, *args, **kwargs ):
		self.alert( *args, type='notice', **kwargs )
