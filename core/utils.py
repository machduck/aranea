#!/usr/bin/env python

from core.models import WebPage, Fuzzable

import re

from itertools import chain
from functools import wraps

def regex( param ):
	def decorator( decoree ):
		@wraps( decoree )
		def wrapper( *args, **kwargs ):
			value = kwargs.get( param, None )
			if value is not None:
				kwargs[param] = re.compile( value )
			else:
				del kwargs[param]
			return decoree( *args, **kwargs )
		return wrapper
	return decorator

def strict_match( string ):
	return '^' + re.escape(string) + '$'

def filter_by_value( dct, good=lambda x: x != None ):
	return filter(lambda (k,v): True if good(v) else False, dct.iteritems())

class Pipeline( object ):

	def __init__( self ):
		self.pipeline = []

	def chain( self, field ):
		self.pipeline.append({'$unwind': '$'+field })

	def select( self, field ):
		self.pipeline.append({'$project': { field: 1 }})

	def count( self, result='total', increment=1 ):
		self.pipeline.append({ '$group': 
			{'_id': None, result:{ '$sum': increment }}
		})

	def regex(self,kwargs,transform=lambda x:x,filter=lambda x:x.iteritems()):
		self.pipeline.append({ '$match':
			dict([ (k,{'$regex':transform(v)}) for (k,v) in filter(kwargs) ])
		})

	def match(self,kwargs,transform=lambda x:x,filter=lambda x:x.iteritems()):
		self.pipeline.append({ '$match':
			dict([ (k,transform(v)) for (k,v) in filter(kwargs) ])
		})

	def __call__( self ):
		return self.pipeline

class Utils( object ):
	NAGLE_RECV = 0x40

	def nagle( self, collection, **kwargs ):
		total = collection.objects( **kwargs ).no_cache().count()
		current = 0
		while current < total:
			for document in collection \
									.objects( **kwargs ) \
									.skip( current ) \
									.limit( Utils.NAGLE_RECV ) \
									.no_cache():
				yield document
			current += Utils.NAGLE_RECV

	@regex( 'host' )
	def requests( self, **kwargs ):
		for page in self.nagle( WebPage, **kwargs ):
			yield page

	@regex( 'host' )
	def fuzzed( self, **kwargs ):
		for fuzzed in self.nagle( Fuzzable, **kwargs ):
			yield fuzzed 

	def delete( self, collection, **kwargs ):
		collection.objects( **kwargs ).delete()

	@regex( 'host' )
	def delete_fuzzed( self, **kwargs ):
		self.delete( Fuzzable, **kwargs )

	@regex( 'host' )
	def delete_requests( self, **kwargs  ):
		self.delete( WebPage, **kwargs )

	def count_hosts( self, **kwargs ):
		return len(self.unique_hosts( **kwargs ))

	def count( self, collection, **kwargs ):
		return collection.objects( **kwargs ).no_cache().count()

	@regex( 'host' )
	def count_requests( self, **kwargs ):
		return self.count( WebPage, **kwargs )

	@regex( 'host' )
	def count_fuzzed( self, **kwargs ):
		return self.count( Fuzzable, **kwargs )

	@regex( 'host' )
	def unique_hosts( self, **kwargs ):
		return WebPage.objects( **kwargs ).no_cache().distinct('host')

	def count_detects( self, **kwargs ):
		pipeline = Pipeline()
		pipeline.regex( kwargs=kwargs,
						transform=strict_match,
						filter=filter_by_value )
		pipeline.select('data')
		pipeline.chain('data')
		pipeline.count()
		query = Fuzzable._get_collection().aggregate(pipeline())
		try:
			return query['result'][0]['total']
		except IndexError:
			return 0
	
	def detects( self, **kwargs ):
		pipeline = Pipeline()
		pipeline.regex( kwargs=kwargs,
						transform=strict_match,
						filter=filter_by_value )
		pipeline.select('data')
		pipeline.chain('data')
		query = Fuzzable._get_collection().aggregate(pipeline())
		return query['result']
