#!/usr/bin/env python

import argparse

from core import db
from core.utils import Utils

def drop( args ):
	utils = Utils()

	if args.collection in [ None, 'requests' ]:
		utils.delete_requests( host=args.host )

	if args.collection in [ None, 'fuzzed' ]:
		utils.delete_fuzzed( host=args.host )

def parse_args( args=None ):
	parser = argparse.ArgumentParser(description='dr0p t3h base')
	parser.add_argument('-u', '--host', help='host', type=str,
						metavar='www.domain.tld', default=None )
	parser.add_argument('-c', '--collection', help='collection', type=str,
						choices=['request', 'fuzzed'], default=None )
	opts = parser.parse_args( args=args )
	return opts

def main(): drop(parse_args())

if __name__ == "__main__":
	main()
