#!/usr/bin/env python

import os
import re
import argparse

from libmproxy import controller, proxy
from libmproxy.encoding import decode, encode

from core import db
from core.models import WebPage

class Decoder( object ):

	@staticmethod
	def decode( *args, **kwargs ):
		return decode( *args, **kwargs )

	@staticmethod
	def encode( *args, **kwargs ):
		return encode( *args, **kwargs )

class LoggingProxy(controller.Master):
	def __init__(self, port=8080, cert='~/.mitmproxy/mitmproxy-ca.pem' ):
		config = proxy.ProxyConfig(cacert = os.path.expanduser(cert))
		server = proxy.ProxyServer(config, port)
		controller.Master.__init__(self, server)

		self.regex = re.compile( re.escape( 'text/html' ), re.I )

	def run(self):
		try:
			return controller.Master.run(self)
		except KeyboardInterrupt:
			self.shutdown()

	def handle_request(self, msg):
		msg.reply()

	def handle_response(self, msg):
		type = WebPage.extract_ct( msg )
		if self.regex.match( type ):
			WebPage.from_proxy( msg, decoder=Decoder ).save()
		msg.reply()

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='mitm logging proxy')
	parser.add_argument('--port', type=int, default=8080 )
	parser.add_argument('--cert', type=str, 
						default='~/.mitmproxy/mitmproxy-ca.pem' )
	args = parser.parse_args()

	LoggingProxy( args.port, args.cert ).run()
