#!/bin/bash

DIR=${PWD}
CWF=${PWD##*/}
DATE=$(date +"%d.%m.%y")
RPATH="/../"

tar cpvz "$DIR" --exclude '*.pyc' --exclude 'venv' > "$DIR$RPATH$CWF-$DATE.tar.gz"
