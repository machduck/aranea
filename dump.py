#!/usr/bin/env python

import argparse

from core import db
from core.utils import Utils, strict_match

def parse_args( args=None ):
	parser = argparse.ArgumentParser(description='all bases r belong to us')
	parser.add_argument('-u', '--host', help='host', type=str,
						metavar='www.domain.tld', default=None )
	parser.add_argument('-d', '--data', help='data', type=str, nargs='+',
						default=[ 'hosts', ],
						metavar=(chr(0x8),'requests, fuzzed, detects, hosts'))
	opts = parser.parse_args( args=args )
	return opts

def dump( args ):
	utils = Utils()

	for host in utils.unique_hosts(host=args.host):
		if 'hosts' in args.data:
			print '[*] [{}]'.format( host )

		if 'requests' in args.data:
			for request in utils.requests(host=strict_match(host)):
				print '[*] {}'.format( request.to_string() )

		for fuzzed in utils.fuzzed(host=strict_match(host)):

			if( 'fuzzed' in args.data
					or ('detects' in args.data
						and 'fuzzed' not in args.data
						and len( fuzzed.data ) > 0 )):

				print '[*] {}'.format( fuzzed.to_string() )

			for data in fuzzed.data:
				if 'detects' in args.data:
					print '[!] {}'.format( data.to_string() )

def main(): dump(parse_args())

if __name__ == "__main__":
	main()
